#!/user/bin/env node

import 'core-js/stable';
import 'regenerator-runtime/runtime';

/**
 * Module dependencies.
 */

import http from 'http';
import app from '../app';

/**
 * Normalize a port into a number, string, or false.
 */
const normalizePort = (val) => {
  const port = parseInt(val, 10);

  if (Number.isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`Listening on ${bind}`);
};

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

const Socket = require('websocket').server;
const webSocket = new Socket({ httpServer: server });

// this will make Express serve your static files
// app.use('/webrtc', express.static(__dirname + '/webrtc'));

const startRTCServer = () => {
  const users = [];
  const waiting = {};

  const findUser = (username) => {
    for (let i = 0; i < users.length; i++) {
      if (users[i].name === username) return users[i];
    }
  };

  webSocket.on('request', (req) => {
    const connection = req.accept();

    connection.on('message', (message) => {
      try {
        console.log(connection.socket._peername);
        console.log(message);
        const data = JSON.parse(message.utf8Data);

        if (data.type !== 'ice_candidate') console.log(data);
        const user = findUser(data.name);

        switch (data.type) {
          case 'store_user':
            if (user != null) {
              //our user exists
              // console.log(connection);
              user.conn = connection;
              connection.send(
                JSON.stringify({
                  type: 'user already exists',
                  data: 'connection is updated'
                }),
              );
              return;
            }

            const newUser = {
              name: data.name,
              conn: connection,
            };
            users.push(newUser);

            connection.send(
              JSON.stringify({
                type: 'welcome',
                data: `Hi ${data.name}, welcome to our server`,
              }),
            );

            if (waiting[data.name]) {
              let userToReceiveNotification = findUser(waiting[data.name]);
              userToReceiveNotification.conn.send(
                JSON.stringify({
                  type: 'partner_online',
                  data: `Hey ${waiting[data.name]}, ${data.name} is online`,
                }),
              );
              delete waiting[data.name];
            }
            break;

          case 'start_call':
            let userToCall = findUser(data.target);

            // userToCall = 11;

            if (userToCall) {
              connection.send(
                JSON.stringify({
                  type: 'call_response',
                  data: 'user is ready for call',
                }),
              );
            } else {
              connection.send(
                JSON.stringify({
                  type: 'call_response',
                  data: 'user is not online',
                }),
              );
            }
            break;

          case 'create_offer':
            let userToReceiveOffer = findUser(data.target);

            console.log('shit , this is the offer description ', data.data);
            if (userToReceiveOffer) {
              userToReceiveOffer.conn.send(
                JSON.stringify({
                  type: 'offer_received',
                  name: data.name,
                  data: data.data.sdp,
                }),
              );
            }
            break;

          case 'create_answer':
            let userToReceiveAnswer = findUser(data.target);
            if (userToReceiveAnswer) {
              userToReceiveAnswer.conn.send(
                JSON.stringify({
                  type: 'answer_received',
                  name: data.name,
                  data: data.data.sdp,
                }),
              );
            }
            break;

          case 'ice_candidate':
            let userToReceiveIceCandidate = findUser(data.target);
            if (userToReceiveIceCandidate) {
              userToReceiveIceCandidate.conn.send(
                JSON.stringify({
                  type: 'ice_candidate',
                  name: data.name,
                  data: {
                    sdpMLineIndex: data.data.sdpMLineIndex,
                    sdpMid: data.data.sdpMid,
                    sdpCandidate: data.data.sdpCandidate,
                  },
                }),
              );
            }
            break;

          case 'create_waiting':
            waiting[data.target] = data.name
            break;
           
        }
      } catch (err) {
        console.log('Error when receiving new message: ', err);
      }
    });

    connection.on('close', () => {
      users.forEach((user) => {
        if (user.conn === connection) {
          users.splice(users.indexOf(user), 1);
          console.log(user.name, ' leaved');
        }
      });
    });
  });
};

startRTCServer();
