var url = new URL(window.location.href);
const myId = url.searchParams.get('id') ?? 'b';

let localVideo = document.getElementById('local-video');
let remoteVideo = document.getElementById('remote-video');

// Let us open a web socket
var ws = new WebSocket('ws://192.168.1.8:3000');
const connections = {
  remote: null,
  local: null,
};
remoteVideo.onplaying = () => {
  remoteVideo.style.opacity = 1;
};

let peer = null;
function init(userId) {
  peer = new Peer(userId, {
    host: '192.168.1.8',
    port: 3001,
    path: '/videocallapp',
  });
}

let remoteStreams = [];

const initConnection = (stream) => {
  ws.send(JSON.stringify(MessageModel('store_user', myId)));

  // Receive Offer From Other Client
  ws.onmessage = function (evt) {
    const message = JSON.parse(evt.data);
    console.log('New message: ', message);

    if (message.type === 'offer_received') {
      const description = message.data;
      // Ininit peer connection
      connections.remote = new RTCPeerConnection();

      connections.remote.onconnectionstatechange = (ev) => {
        switch (connections.remote.connectionState) {
          case 'disconnected':
          case 'closed':
          case 'failed':
            console.log('RTC connection finished!');
            window.location.reload();
            break;
          default:
            break;
        }
      };

      // Add all tracks from stream to peer connection
      stream.getTracks().forEach((track) => {
        console.log('NEw track from local ', track);
        connections.remote.addTrack(track, stream);
      });

      // Send Candidtates to establish a channel communication to send stream and data
      connections.remote.onicecandidate = ({ candidate }) => {
        if (candidate) {
          let candidateObj = candidate.toJSON();
          candidateObj = {
            sdpCandidate: candidateObj.candidate,
            sdpMid: candidateObj.sdpMid,
            sdpMLineIndex: candidateObj.sdpMLineIndex,
          };
          console.log('Got a new ice candidate ', candidateObj);
          ws.send(
            JSON.stringify(
              MessageModel('ice_candidate', myId, message.name, candidateObj),
            ),
          );
        }
        // candidate && socket.emit("candidate", socketId, candidate);
      };

      // Receive stream from remote client and add to remote video area
      connections.remote.ontrack = ({ streams: [stream] }) => {
        remoteStreams.push(stream);

        if (remoteStreams.length == 1) {
          remoteVideo.srcObject = stream;
          remoteVideo.play();

          remoteVideo.className = 'primary-video';
          localVideo.className = 'secondary-video';
        }
      };

      // Set Local And Remote description and create answer

      const offerRTCSessionDescription = new RTCSessionDescription();
      offerRTCSessionDescription.type = 'offer';
      offerRTCSessionDescription.sdp = message.data;

      // connections.remote.addStream(stream);

      connections.remote
        .setRemoteDescription(offerRTCSessionDescription)
        .then(() => connections.remote.createAnswer())
        .then((answer) => {
          console.log('Local description: ', answer);
          return connections.remote.setLocalDescription(answer);
        })
        .then(() => {
          console.log('Already set descriptions');
          ws.send(
            JSON.stringify(
              MessageModel(
                'create_answer',
                myId,
                message.name,
                connections.remote.localDescription,
              ),
            ),
          );
        });
    } else if (message.type == 'answer_received') {
      const offerRTCSessionDescription = new RTCSessionDescription();
      offerRTCSessionDescription.type = 'answer';
      offerRTCSessionDescription.sdp = message.data;

      connections.local.setRemoteDescription(offerRTCSessionDescription);
    } else if (message.type == 'ice_candidate') {
      // GET Local or Remote Connection
      console.log(message.data);
      connections.remote.addIceCandidate(new RTCIceCandidate(message.data));
    }
  };
};

ws.onopen = function () {
  // Web Socket is connected, send data using send()
  console.log('WebRTC server connected!');
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: true })
    .then((stream) => {
      // Show My Video
      localVideo.srcObject = stream;

      // Start a Peer Connection to Transmit Stream
      initConnection(stream);
    })
    .catch((error) => console.log(error));
};

ws.onclose = function () {
  // websocket is closed.
  console.log('Websocket connection is closed...');
};

localVideo.style.opacity = 0;
remoteVideo.style.opacity = 0;

localVideo.onplaying = () => {
  localVideo.style.opacity = 1;
};
remoteVideo.onplaying = () => {
  remoteVideo.style.opacity = 1;
};

function MessageModel(type, name, target, data) {
  const message = {
    type: type,
    name: name,
    target: target,
    data: data,
  };

  console.log(JSON.stringify(message));

  return message;
}
