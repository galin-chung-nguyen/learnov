import express from 'express';
import {
  db,
  sendNotifications,
  sessionRequestsList,
  sessionsList,
  welcomeLogin,
  getUser,
  getDoc,
  startSession,
} from './startFirebase';
import { SESSION_REQUEST_STATE, SESSION_STATE } from './_constants';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json({
    status: 200,
    message: 'success',
  });
});

router.post('/new-session-request', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();
  const question = (req.body.question || '').trim();
  const sharedLink = (req.body.sharedLink || '').trim();

  console.log(req.body);

  try {
    if (!userId || !question) {
      throw new Error('UserId and question must not be empty!');
    }

    const userData = await getUser(userId);
    if (!userData.data()) throw new Error(`UserId #${userId} is not valid`);

    console.log(userId);
    console.log(question);
    console.log(sharedLink);

    if (userData.data().mode !== 'learner')
      throw new Error('You are not in learner mode');

    const currentQuestion = sessionRequestsList.getQuestion(userId);

    console.log(currentQuestion);

    if (
      currentQuestion &&
      (currentQuestion.status === SESSION_REQUEST_STATE.PENDING ||
        currentQuestion.status === SESSION_REQUEST_STATE.INSESSION)
    ) {
      throw new Error('You have already requested a tutoring session');
    }

    // request new session
    // add to firebase
    const sessionRequestRef = (
      await db.collection('session-requests').add({
        'learner-id': userId,
        status: SESSION_REQUEST_STATE.PENDING,
        question: question,
        sharedLink: sharedLink,
      })
    ).id;

    sessionRequestsList.addQuestion(userId, sessionRequestRef, question, sharedLink, "PENDING");

    await db.collection('users').doc(userId).update({
      currentSessionRequestId: sessionRequestRef,
    });

    // send notification to all available tutors
    await sendNotifications({
      notification: {
        title: `New question from learner ${userData.data().name}`,
        body: question,
      },
      data: {
        learnerId: userId,
        question: question,
        sharedLink: sharedLink,
        type: 'new-session-request',
      },
      topic: 'learner-question',
    });

    res.json({
      status: 200,
      message: 'success',
      data: {
        sessionRequestId: sessionRequestRef,
      },
    });
  } catch (err) {
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

// for tutors
router.post('/accept-session-request', async (req, res, next) => {
  const tutorId = (req.body.tutorId || '').trim();
  const sessionRequestId = (req.body.sessionRequestId || '').trim();

  console.log(req.body);

  console.log(tutorId, ' ', sessionRequestId);
  try {
    if (!tutorId || !sessionRequestId) {
      throw new Error('tutorId and sessionRequestId must not be empty!');
    }

    const tutorData = (await getDoc('users', tutorId)).data();
    if (!tutorData) throw new Error(`tutorId #${tutorId} is not valid`);

    if (tutorData.mode !== 'tutor')
      throw new Error('You are not in tutor mode');

    const learnerId =
      sessionRequestsList.checkPendingQuestion(sessionRequestId);
    if (!learnerId)
      throw new Error(`Session request #${sessionRequestId} is not open`);

    if (tutorId == learnerId)
      throw new Error('You cannot accept your own session request!'); // This cannot happen, unless the user is cheating

    const learnerData = (await getDoc('users', learnerId)).data();

    const sessionRequestData = (
      await getDoc('session-requests', sessionRequestId)
    ).data();

    // update status of the session request on firebase
    sessionRequestsList.updateQuestionStatus(
      learnerId,
      SESSION_REQUEST_STATE.INSESSION,
    );
    await db.collection('session-requests').doc(sessionRequestId).update({
      status: SESSION_REQUEST_STATE.INSESSION,
    });

    const learnerConnId = uuidv4(),
      tutorConnId = uuidv4();
    // create new session
    const sessionId = (
      await db.collection('sessions').add({
        sessionRequestId: sessionRequestId,
        question: sessionRequestData.question,
        learnerId: sessionRequestData['learner-id'],
        tutorId: tutorId,
        learnerConnId,
        tutorConnId,
        createdDate: new Date(),
        duration: -1,
        learnerRate: -1,
        tutorRate: -1,
        status: SESSION_STATE.INSESSION,
      })
    ).id;

    await db.collection('users').doc(learnerId).update({
      currentSessionId: sessionId,
      connId: learnerConnId,
    });

    await db.collection('users').doc(tutorId).update({
      currentSessionId: sessionId,
      connId: tutorConnId,
    });

    // sessionsList.addQuestion(tutorId, sessionRequestRef, question);

    // send notification to all available tutors

    const question = sessionRequestData.question;

    // send to learner
    sendNotifications({
      notification: {
        title: `New session with tutor ${tutorData.name} started`,
        body: `Get into the session with tutor about the question '${question}' now.`,
      },
      data: {
        tutorId: tutorId,
        question: question,
        sessionId,
        type: 'new-session-started',
      },
      token: learnerData.FCMToken,
      // topic: 'learner-session',
    });

    sendNotifications({
      data: {
        tutorId: tutorId,
        question: question,
        sessionId,
        connId: learnerConnId,
        targetConnId: tutorConnId,
        type: 'new-session-started.learner',
      },
      token: learnerData.FCMToken,
      // topic: 'learner-session',
    });
 
    // send to tutor
    sendNotifications({
      notification: {
        title: `New session with learner ${learnerData.name} started`,
        body: `Get into the session to help learner resolve the question '${question}' now.`,
      },
      data: {
        learnerId: learnerId,
        question: question,
        sessionId,
        type: 'new-session-started',
      },
      // topic: 'tutor-session',
      token: tutorData.FCMToken,
    });
    sendNotifications({
      data: {
        learnerId: learnerId,
        question: question,
        sessionId,
        connId: tutorConnId,
        targetConnId: learnerConnId,
        type: 'new-session-started.tutor',
      },
      // topic: 'tutor-session',
      token: tutorData.FCMToken,
    });
    startSession(sessionId, learnerId, tutorId);

    res.json({
      status: 200,
      message: 'success',
      data: {
        sessionId,
      },
    });
  } catch (err) {
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

// for tutors
router.post('/cancel-session-request', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();

  console.log(userId);
  try {
    if (!userId) {
      throw new Error('userId must not be empty!');
    }

    const learnerData = (await getDoc('users', userId)).data();
    if (!learnerData) throw new Error(`userId #${userId} is not valid`);

    if (learnerData.mode !== 'learner')
      throw new Error('You are not in learner mode');

    if (
      !sessionRequestsList.getQuestion(userId) ||
      !sessionRequestsList.getQuestion(userId).requestId
    )
      throw new Error('You are not having any session request');

    const sessionRequestId = sessionRequestsList.getQuestion(userId).requestId;

    const sessionRequestData = (
      await getDoc('session-requests', sessionRequestId)
    ).data();

    if (sessionRequestData.status !== SESSION_REQUEST_STATE.PENDING) {
      throw new Error(`Session request must be in PENDING mode`);
    }

    // update status of the session request on firebase
    console.log(sessionRequestData);
    console.log(sessionRequestId);
    await db.collection('session-requests').doc(sessionRequestId).update({
      status: SESSION_REQUEST_STATE.CANCELED,
    });

    await db.collection('users').doc(userId).update({
      currentSessionRequestId: '',
      currentSessionId: '',
      connId: '',
    });

    res.json({
      status: 200,
      message: 'success',
      data: {
        sessionRequestId,
      },
    });
  } catch (err) {
    console.log(err);
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

// for tutors
router.post('/get-session-history', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();

  console.log(userId);
  try {
    if (!userId) {
      throw new Error('userId must not be empty!');
    }

    const learnerData = (await getDoc('users', userId)).data();
    if (!learnerData) throw new Error(`userId #${userId} is not valid`);

    let result = [];

    let snapshotLearner = await db
      .collection('sessions')
      .where('learnerId', '==', userId)
      .get();

    snapshotLearner.forEach((doc) => {
      result.push({
        ...doc.data(),
        id: doc.id,
      });
    });

    let snapshotTutor = await db
      .collection('sessions')
      .where('tutorId', '==', userId)
      .get();
    snapshotTutor.forEach((doc) => {
      result.push({
        ...doc.data(),
        id: doc.id,
      });
    });

    res.json({
      status: 200,
      message: 'success',
      data: {
        sessions: result,
      },
    });
  } catch (err) {
    console.log(err);
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

router.post('/stop-session', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();

  try {
    if (!userId) {
      throw new Error('userId must not be empty!');
    }
    const userData = (await getDoc('users', userId)).data();
    if (!userData) throw new Error(`userId #${userId} is not valid`);

    let sessionId = sessionsList.IsInSession(userId);
    if (!sessionId) {
      throw new Error('You are not joining any tutoring session');
    }
    let isLearner = sessionsList.sessionsList[sessionId].learnerId === userId;

    const otherUserId = isLearner
      ? sessionsList.sessionsList[sessionId].tutorId
      : sessionsList.sessionsList[sessionId].learnerId;
    const otherUserData = (await getDoc('users', otherUserId)).data();

    const sessionRequestId = (
      sessionRequestsList.getQuestion(userId) ||
      sessionRequestsList.getQuestion(otherUserId)
    ).requestId;

    console.log(
      userId,
      ' ',
      otherUserId,
      ' => ',
      sessionId,
      ' / ',
      sessionRequestId,
    );
    // console.log('isLeaner ', isLearner);
    // console.log(sessionRequestId);
    // console.log(otherUserData, ' ', otherUserId);

    // throw new Error('dcm');

    if (sessionRequestId)
      await db.collection('session-requests').doc(sessionRequestId).update({
        status: SESSION_REQUEST_STATE.FINISHED,
      });

    await db
      .collection('sessions')
      .doc(sessionId)
      .update({
        status: SESSION_STATE.FINISHED,
        duration:
          new Date().getTime() -
          new Date(
            sessionsList.sessionsList[sessionId].createdDate.seconds * 1000,
          ).getTime(),
      });

    await db.collection('users').doc(userId).update({
      currentSessionId: '',
      currentSessionRequestId: '',
      connId: '',
    });

    await db.collection('users').doc(otherUserId).update({
      currentSessionId: '',
      currentSessionRequestId: '',
      connId: '',
    });

    // Send session finished successfully
    sendNotifications({
      data: {
        sessionId,
        type: 'session-finished',
      },
      token: userData.FCMToken,
      // topic: 'learner-session',
    });

    sendNotifications({
      data: {
        sessionId,
        type: 'session-finished',
      },
      token: otherUserData.FCMToken,
      // topic: 'learner-session',
    });
    
    res.json({
      status: 200,
      message: 'success',
      data: {
        sessionId,
      },
    });
  } catch (err) {
    console.log(err);
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

router.post('/rate-session', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();
  const sessionId = (req.body.sessionId || '').trim();
  const learnerRate = (req.body.learnerRate || -1);

  console.log("1: ", sessionsList.sessionsList);

  console.log('userId ' + userId);
  console.log('sessionId ' + sessionId);
  console.log('learnerRate ' + learnerRate);

  try {
    if (!userId) {
      throw new Error('userId must not be empty!');
    }
    const userData = (await getDoc('users', userId)).data();
    if (!userData) throw new Error(`userId #${userId} is not valid`);

    const session = (await getDoc('sessions', sessionId)).data();
    if (!session) {
      throw new Error('Session not found');
    }

    let isLearner = session.learnerId === userId;
    if (!isLearner) {
      throw new Error('Only learner of this session is allowed to rate');
    }

    let isRated = session.learnerRate !== -1;
    if (isRated) {
      throw new Error('This session is rated');
    }

    const otherUserId = session.tutorId;
    const otherUserData = (await getDoc('users', otherUserId)).data();

    await db.collection('sessions').doc(sessionId).update({
      learnerRate: learnerRate,
    });

    console.log(
      userId,
      ' rate ',
      otherUserId,
      ' => ',
      sessionId,
      ' ',
      learnerRate
    );
    
    // Send rates to tutor
    sendNotifications({
      data: {
        sessionId,
        learnerRate: learnerRate.toString(),
        type: 'rating-received.tutor',
      },
      token: otherUserData.FCMToken,
      // topic: 'learner-session',
    });
    
    res.json({
      status: 200,
      message: 'success',
      data: {
        sessionId,
      },
    });
  } catch (err) {
    console.log(err);
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

export default router;
