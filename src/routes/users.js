import { async } from '@firebase/util';
import express from 'express';
import {
  db,
  sendNotifications,
  sessionRequestsList,
  welcomeLogin,
  getUser,
  getDoc,
  startSession,
  sessionsList,
} from './startFirebase';
import { SESSION_REQUEST_STATE } from './_constants';

const router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

router.post('/new-login', (req, res, next) => {
  const userId = req.body.userId;

  if (userId) {
    setTimeout(() => {
      welcomeLogin(userId);
      res.json({
        status: 200,
      });
    }, 2000);
  } else {
    res.json({
      status: 400,
    });
  }
});

router.post('/switch-user-mode', async (req, res, next) => {
  const userId = (req.body.userId || '').trim();

  try {
    if (!userId) {
      throw new Error('userId must not be empty!');
    }

    const userData = (await getDoc('users', userId)).data();
    if (!userData) throw new Error(`userId #${userId} is not valid`);
    if (sessionsList.IsInSession(userId)) {
      throw new Error(
        'You are having a tutoring session. Please exit the session first.',
      );
    } else {
      const curSessionRequest = sessionRequestsList.getQuestion(userId);

      if (
        curSessionRequest &&
        curSessionRequest.status !== SESSION_REQUEST_STATE.FINISHED
      ) {
        throw new Error(
          'You are having a tutoring session request. Please cancel it first.',
        );
      }
    }

    const newMode = userData.mode === 'learner' ? 'tutor' : 'learner';
    await db.collection('users').doc(userId).update({
      mode: newMode,
    });

    res.json({
      status: 200,
      message: 'success',
      data: {
        mode: newMode,
      },
    });
  } catch (err) {
    res.json({
      status: 400,
      message: typeof err == 'string' ? err : err?.message ?? toString(err),
    });
  }
});

export default router;
