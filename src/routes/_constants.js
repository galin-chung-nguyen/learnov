export const SESSION_REQUEST_STATE = {
  PENDING: 'PENDING',
  CANCELED: 'CANCELED',
  INSESSION: 'INSESSION',
  FINISHED: 'FINISHED',
  ERROR: 'ERROR',
};

export const SESSION_STATE = {
  INSESSION: 'INSESSION',
  CANCELED: 'CANCELED',
  FINISHED: 'FINISHED',
  ERROR: 'ERROR',
};
