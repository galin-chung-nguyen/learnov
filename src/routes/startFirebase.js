const admin = require('firebase-admin');
import { SESSION_REQUEST_STATE, SESSION_STATE } from './_constants';

const serviceAccount = require('./learnov-app-firebase-adminsdk-d6ldn-d960278b16.json');

// const app = admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL:
//     'https://learnov-app-default-rtdb.asia-southeast1.firebasedatabase.app',
// });

const {
  initializeApp,
  applicationDefault,
  cert,
} = require('firebase-admin/app');
const {
  getFirestore,
  Timestamp,
  FieldValue,
} = require('firebase-admin/firestore');

const { getMessaging } = require('firebase-admin/messaging');

initializeApp({
  credential: cert(serviceAccount),
});

const db = getFirestore();

const sendNotifications = (message) => {
  return getMessaging().send(message);
};

const getUser = async (userId) => {
  return db.collection('users').doc(userId).get();
};

const getDoc = async (collectionName, docId) => {
  return db.collection(collectionName).doc(docId).get();
};
const welcomeLogin = async (userId) => {
  getUser(userId).then((user) => {
    const userData = user.data();
    const FCMToken = userData.FCMToken;
    console.log(userData);

    const message = {
      notification: {
        title: `Welcome to Learnov, ${userData.name}!`,
        body: '',
      },
      token: FCMToken,
    };

    // Send a message to the device corresponding to the provided
    // registration token.
    getMessaging()
      .send(message)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent new-login notification:', response);
      })
      .catch((error) => {
        console.log('Error sending new-login notification:', error);
      });
  });
};

const startFirebase = async () => {
  {
  }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
class OnlineUsersList {
  constructor() {
    this.onlineUsers = {};
  }
  updateOnlineUsersList(_newList) {
    this.onlineUsers = _newList;
  }
}

const onlineUsersList = new OnlineUsersList();

const updateOnlineUsersList = () => {
  setInterval(() => {
    db.collection('online-users')
      .get()
      .then((querySnapshot) => {
        // console.log('Reset online users');
        const newOnlineUsersList = {};
        const currentTime = new Date();

        querySnapshot.forEach((doc) => {
          let lastUpdateTime =
            doc.data()['last-update'].toDate() || new Date(0);

          // console.log(
          //   lastUpdateTime.toISOString(),
          //   ' => ',
          //   currentTime.toISOString(),
          // );
          if ((currentTime.getTime() - lastUpdateTime.getTime()) / 1000 < 60) {
            newOnlineUsersList[doc.id] = doc.data();
          }
        });

        onlineUsersList.updateOnlineUsersList(newOnlineUsersList);
        // console.log(onlineUsersList.onlineUsers);
      })
      .catch((err) => {
        console.log('Error when updaing online users list: ', err);
      });
  }, 10000);
};
updateOnlineUsersList();

//////////////////////////////////////////////////////////////////////////////////////////////////////
class SessionRequestList {
  constructor() {
    this.sessionRequestsList = {};
  }

  static obj = null;

  static getSessionRequestList() {
    if (SessionRequestList.obj === null) {
      SessionRequestList.obj = new SessionRequestList();

      db.collection('session-requests').onSnapshot((querySnapshot) => {
        SessionRequestList.obj.updateSessionRequestsList({});

        querySnapshot.forEach((data) => {
          const requestData = data.data();
          if (
            requestData.status.trim().toUpperCase() !==
              SESSION_REQUEST_STATE.FINISHED &&
            requestData.status.trim().toUpperCase() !==
              SESSION_REQUEST_STATE.CANCELED
          ) {
            SessionRequestList.obj.addQuestion(
              requestData['learner-id'],
              data.id,
              requestData.question,
              requestData.sharedLink,
              requestData.status,
            );
          }
        });
      });
    }

    return SessionRequestList.obj;
  }
  updateSessionRequestsList(_newList) {
    this.sessionRequestsList = _newList;
  }

  getQuestion(userId) {
    return this.sessionRequestsList.hasOwnProperty(userId)
      ? this.sessionRequestsList[userId]
      : null;
  }

  addQuestion(userId, requestId, question, sharedLink, status) {
    this.sessionRequestsList[userId] = {
      requestId,
      question,
      sharedLink,
      status,
    };
  }

  updateQuestionStatus(userId, status) {
    this.sessionRequestsList[userId].status = status;
  }

  // check if a session request is still pending and return the corresponding learnId
  checkPendingQuestion(requestId) {
    let result = false;
    let learnerId = null;

    for (let userId in this.sessionRequestsList) {
      let val = this.sessionRequestsList[userId];
      if (
        val.requestId == requestId &&
        val.status == SESSION_REQUEST_STATE.PENDING
      ) {
        result = true;
        learnerId = userId;
        break;
      }
    }
    return result ? learnerId : null;
  }

  removeQuestion(userId) {
    delete this.sessionRequestsList[userId];
  }
}
const sessionRequestsList = new SessionRequestList.getSessionRequestList();

//////////////////////////////////////////////////////////////////////////////////////////////////////

class SessionList {
  constructor() {
    this.sessionsList = {};
  }

  static obj = null;

  static getSessionList() {
    if (SessionList.obj === null) {
      SessionList.obj = new SessionList();

      db.collection('sessions').onSnapshot((querySnapshot) => {
        SessionList.obj.updateSessionsList({});
        querySnapshot.forEach((data) => {
          const requestData = data.data();
          try {
            const currenStatus = (requestData.status || '')
              ?.trim()
              .toUpperCase();
            if (currenStatus !== SESSION_STATE.FINISHED) {
              SessionList.obj.addSession(data.id, {
                learnerId: requestData['learnerId'],
                tutorId: requestData['tutorId'],
                question: requestData['question'],
                createdDate: requestData['createdDate'],
                duration: requestData['duration'],
                learnerRate: requestData['learnerRate'],
                tutorRate: requestData['tutorRate'],
                learnerConnId: requestData['learnerConnId'],
                tutorConnId: requestData['tutorConnId'],
                status: requestData['status'],
              });
            }
          } catch (err) {
            console.log('Error ');
            console.log(err.message);
          }
        });

        querySnapshot.docChanges().forEach(function (change) {
          if (change.type === 'removed') {
            delete SessionList[change.doc.id];
          }
        });
      });
    }

    return SessionList.obj;
  }
  updateSessionsList(_newList) {
    this.sessionsList = _newList;
  }

  IsInSession(userId) {
    for (let sessionId in this.sessionsList) {
      let val = this.sessionsList[sessionId];
      if (val.status !== SESSION_STATE.FINISHED) {
        if (val.learnerId === userId || val.tutorId === userId) {
          return sessionId;
        }
      }
    }
    return null;
  }

  IsUserRated(sessionId) {
    return this.sessionsList[sessionId].learnerRate !== -1;
  }

  addSession(sessionId, data) {
    this.sessionsList[sessionId] = data;
  }

  updateSessionStatus(sessionId, status) {
    this.sessionsList[sessionId].status = status;
  }

  removeSession(sessionId) {
    delete this.sessionsList[sessionId];
  }
}
const sessionsList = new SessionList.getSessionList();

const startSession = async (sessionId, learnerId, tutorId) => {
  console.log('start new session ', sessionId);
};

setInterval(() => {
  console.log(sessionRequestsList.sessionRequestsList);
  console.log(sessionsList.sessionsList);
}, 10000);
//////////////////////////////////////////////////////////////////////////////////////////////////////

exports.sendNotifications = sendNotifications;
exports.db = db;
exports.sessionRequestsList = sessionRequestsList;
exports.sessionsList = sessionsList;
exports.onlineUsers = onlineUsersList;
exports.welcomeLogin = welcomeLogin;
exports.startFirebase = startFirebase;
exports.getUser = getUser;
exports.getDoc = getDoc;
exports.startSession = startSession;
