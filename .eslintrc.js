module.exports = {
  env: {
    es2021: true,
    commonjs: true,
  },
  extends: [
    'airbnb',
    'plugin:import/typescript', // this is needed because airbnb uses eslint-plugin-import
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint', // not related to this problem but it helps
    'prettier/react',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'prettier/prettier': 'error',
  },
  plugins: ['prettier'],
  settings: {
    'import/resolver': {
      'babel-module': {},
    },
  },
};
