## Learnov

**Learnov** is an application that helps connect English learners (learners) with English tutors (tutors). Learner just needs to ask a question and press the search button, the system will automatically send a notification to the tutor and a session will start as soon as a tutor accepts a tutor request from a learner.

During the session, learner and tutor can call each other using a video call feature built using **WebRTC** technology. The application also allows users to draw and add special effects / filters to their frames through **AR** technology. At the same time, users can see the satisfaction of the other person during the video call session through **AI** artificial intelligence to help identify emotions on the face.

There is also an English knowledge exchange forum as well as other basic features to help create the most complete and smooth user experience.

The app was built with Java for Android UI, Firebase for database storage and push notification, AI model for face recognition and emotion detection in video, AR Sceneform for sticker and drawing on video call, NodeJS and Express for back-end and WebRTC protocol for video call.

Below are some demo photos of this app:

##### Detect the learner's face and classify the learner's emotions from the face using AI model with Google ML Kit, Tensorflow Lite

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.005.jpeg)![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.006.jpeg) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.007.jpeg)

##### Video filter with AR Sceneform

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.008.jpeg)![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.009.jpeg) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.010.jpeg)

##### Video call

![](hreport/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.011.jpeg) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.012.png)

##### Sign in with Google

![img](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.013.png)

##### Post a question on forum

![img](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.014.png) .![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.015.png)

**See questions on forum**

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.016.png)

##### Answer/React to a question on forum

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.017.png) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.018.jpeg)

**Share a forum question to facebook**

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.019.png) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.020.jpeg)

See session history

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.021.png)

##### Switch user mode (tutor <-> learner)

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.022.png)

##### Attach file into a description when creating tutor matching request (File is automatically upload onto Google Drive)

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.023.jpeg)

##### Notifications for new tutor matching order from users

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.024.png)

**See tutor matching request of learners**

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.025.png) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.026.png) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.027.jpeg)

##### Turn on/off AR feature

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.028.jpeg) ![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.029.jpeg)

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.030.jpeg)

##### Profile page

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.031.png)

##### Tutor performance rating system

![](report/images/Aspose.Words.9e6445cd-1c3d-4609-818a-e1b6b0ec11d2.032.jpeg)

## Contributors

Thanks to the following people who have contributed to this project:

- [@loc4atnt](https://github.com/loc4atnt) 📖
- [@trislee02](https://github.com/trislee02) 🐛

## Contact

If you want to contact me you can reach out to me at [Linkedin](https://www.linkedin.com/in/galin-chung-nguyen/).

### **Have a good day!**
